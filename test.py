import string

from automat_finit import AF

af = AF()
af.read_from_file('basic_integer.txt')

for number in range(1000):
    assert af.check(str(number))

for letter in string.ascii_letters:
    assert not af.check(letter)

assert not af.check('123a')
assert not af.check('a123')
assert not af.check('12_')
assert not af.check('0123')
assert not af.check('0000')

assert af.cel_mai_lung_prefix('1234a1234') == '1234'
assert af.cel_mai_lung_prefix('1abcd') == '1'
assert af.cel_mai_lung_prefix('abcd') == ''
assert af.cel_mai_lung_prefix('0213') == '0'

# secventa intreaga e considerata prefix?
assert af.cel_mai_lung_prefix('1234') == '123'   # Nu
# assert af.cel_mai_lung_prefix('1234') == '1234'  # Da
