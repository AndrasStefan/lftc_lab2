from automat_finit import AF

af = AF()
af.read_from_file('basic_integer.txt')


def print_meniu():
    print()
    print('*' * 20)
    print('0. Quit')
    print('1. Multimea starilor: ')
    print('2. Alfabetul: ')
    print('3. Tranzitiile: ')
    print('4. Multimea starilor finale: ')
    print('5. Verifica secventa: ')
    print('6. Cel mai lung prefix acceptat: ')
    print('*' * 20)


while True:
    print_meniu()
    command = input('Comanda este: ')
    if command == '1':
        print(af.stari)
    elif command == '2':
        print(af.alfabet)
    elif command == '3':
        for tranz in af.tranzitii:
            print(tranz)
    elif command == '4':
        print(af.stari_finale)
    elif command == '5':
        secventa = input('Secventa este: ')
        print(af.check(secventa))
    elif command == '6':
        secventa = input('Secventa este: ')
        print(af.cel_mai_lung_prefix(secventa))
    elif command == '0':
        break
    else:
        print('Comanda invalida')

